import { Pipe, PipeTransform } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
    name: 'html'
})
export class HtmlPipe implements PipeTransform {

  //将DomSanitizer添加到构造方法中
    constructor(private _sanitizer: DomSanitizer){}
    transform(value: any, ...args: any[]): any {
      //这里返回的一个html内容，将value进行转换
        return this._sanitizer.bypassSecurityTrustHtml(value);
    }
}
