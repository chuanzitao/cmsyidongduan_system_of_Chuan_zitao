import { Injectable } from '@angular/core';
import {ToastController} from "@ionic/angular";

@Injectable({
  providedIn: 'root'
})
export class ToastService {
                    //构造方法注入ToastController
    constructor(private toastController: ToastController) { }

    //成功提示信息
    public async showSuccessToast(msg:string){
        const toast = await this.toastController.create({
            message: msg,
            duration: 4000,  //出现的时间
            color:"success",
            position:"middle"  //在窗口的显示位置
        });
        toast.present();
    }


    //警告提示信息
    public async showWarningToast(msg:string){
        const toast = await this.toastController.create({
            message: msg,
            duration: 4000,  //出现的时间
            color:"warning",
            position:"middle"  //在窗口的显示位置
        });
        toast.present();
    }

    //警错误提示信息
    public async showErrrorToast(msg:string){
        const toast = await this.toastController.create({
            message: msg,
            duration: 4000,  //出现的时间
            color:"danger",
            position:"middle"  //在窗口的显示位置
        });
        toast.present();
    }


}