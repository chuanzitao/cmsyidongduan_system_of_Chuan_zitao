import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MyFriend} from '../model/MyFriend';

@Injectable({
  providedIn: 'root'
})
export class MyfriendService {

    constructor(private http:HttpClient) { }
    private HOST="https://blog.mzwhzy.com";


    private saveMyfrindUrl=this.HOST+"/public/saveMyFrinds";
    saveMyFriends(myfriends:MyFriend){

        return this.http.post(this.saveMyfrindUrl,myfriends).toPromise();
    }


    private deteMyFriendsUrl=this.HOST+"/public/deteMyFriends";
    deleteMyFriends(id:string){
        let parm={
            "myfriendid":id
        }

        return this.http.post(this.deteMyFriendsUrl,parm).toPromise();

    }

    private getSingleMyFriendsUrl=this.HOST+"/public/getSingleMyFriends";
    getSingleMyFriends(id:string){

        let parm={
            "myfriendid":id
        }

        return this.http.post(this.getSingleMyFriendsUrl,parm).toPromise();
    }

    private getMyFriendsListUrl=this.HOST+"/public/getLatestMyFriends";
    getMyFriendsList(){
        return this.http.get(this.getMyFriendsListUrl).toPromise();
    }
}
