import { Injectable } from '@angular/core';
import {ConfigService} from './config.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SinglepageService {

    //构造方法引入ConfigService和HttpClient
    constructor(private config:ConfigService,
                private http:HttpClient) { }

    private getSinglePageListUrl=this.config.HOST+"/public/getSinglePageList";
    getSinglePageList(){
        return this.http.get(this.getSinglePageListUrl).toPromise();
    }


    private getSinglePageUrl=this.config.HOST+"/public/getSinglePageById";
    getSinglePage(id:string){
        let parm={
            "singlepageid":id
        }
        return this.http.post(this.getSinglePageUrl,parm).toPromise();
    }

}
