export class Singlepage{
    singlepageid:string;
    title:string;
    author:string;
    pbdate:string;
    content:string;
    cover:string;
    describe:string;
}