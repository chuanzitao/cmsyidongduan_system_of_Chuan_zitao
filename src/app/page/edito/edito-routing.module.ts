import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditoPage } from './edito.page';

const routes: Routes = [
  {
    path: '',
    component: EditoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditoPageRoutingModule {}
