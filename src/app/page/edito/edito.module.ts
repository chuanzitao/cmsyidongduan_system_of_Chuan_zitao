import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditoPageRoutingModule } from './edito-routing.module';

import { EditoPage } from './edito.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditoPageRoutingModule
  ],
  declarations: [EditoPage]
})
export class EditoPageModule {}
