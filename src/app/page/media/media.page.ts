import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MediaService} from '../../service/media.service';
import {Media} from '../../model/Media';

@Component({
  selector: 'app-media',
  templateUrl: './media.page.html',
  styleUrls: ['./media.page.scss'],
})
export class MediaPage implements OnInit {

    m:Media;
    mediaid:string;
    constructor(private parm:ActivatedRoute,private mediaservice:MediaService) {
        this.m=new Media();
        this.mediaid=this.parm.snapshot.paramMap.get("mediaid");
        if(this.mediaid){
            this.getMediaListId(this.mediaid);
        }
    }


    ngOnInit() {
  }

    getMediaListId(id:string){

        this.mediaservice.getMediaListById(id)
            .then((data:any)=>{
                this.m=new Media();
                this.m=data;
            })

    }

}
