import { Component, OnInit } from '@angular/core';
import {Singlepage} from '../../model/Singlepage';
import {ActivatedRoute} from '@angular/router';
import {SinglepageService} from '../../service/singlepage.service';

@Component({
  selector: 'app-singlepagedetail',
  templateUrl: './singlepagedetail.page.html',
  styleUrls: ['./singlepagedetail.page.scss'],
})
export class SinglepagedetailPage implements OnInit {
    id:string;
    singlepage:Singlepage;
  constructor(private parm:ActivatedRoute,
              private singlepageservice:SinglepageService) {
      this.singlepage=new Singlepage();
      //接收URL参数
      this.id=this.parm.snapshot.paramMap.get("id");
  }

  ngOnInit() {
  }

    //当页面加载时运行
    ionViewWillEnter(){

        this.loadSinglepageById();

    }
    //从服务器端获取单个新闻对象数据
    loadSinglepageById(){

        this.singlepageservice.getSinglePage(this.id)
            .then((data:any)=>{
                this.singlepage=new Singlepage();
                this.singlepage=data;


            })


    }

}
