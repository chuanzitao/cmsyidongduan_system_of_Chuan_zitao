import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SinglepagedetailPage } from './singlepagedetail.page';

const routes: Routes = [
  {
    path: '',
    component: SinglepagedetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SinglepagedetailPageRoutingModule {}
