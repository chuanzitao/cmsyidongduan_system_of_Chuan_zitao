import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SinglepagedetailPageRoutingModule } from './singlepagedetail-routing.module';

import { SinglepagedetailPage } from './singlepagedetail.page';
import {PipesModule} from '../../pipes/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SinglepagedetailPageRoutingModule,
      PipesModule
  ],
  declarations: [SinglepagedetailPage]
})
export class SinglepagedetailPageModule {}
