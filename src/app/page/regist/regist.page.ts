import { Component, OnInit } from '@angular/core';
import {Users} from '../../model/Users';
import {AuthService} from "../../service/auth.service";
import {ToastService} from "../../service/toast.service";
import {LoginComponent} from '../../components/login/login.component';
import {ModalController, NavController} from '@ionic/angular';
import {ModalOptions} from '@ionic/core'
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-regist',
  templateUrl: './regist.page.html',
  styleUrls: ['./regist.page.scss'],
})
export class RegistPage implements OnInit {
    user:Users;
  constructor(private authservice:AuthService
      ,private toastservice:ToastService
  ,public modalController: ModalController
    ,public navCtrl: NavController) {
      this.user=new Users();

  }

  ngOnInit() {
  }

  //保存方法
    save(){
        console.dir(this.user);

        this.authservice.addOrUpdateUser(this.user)
            .then((data:any)=>{
                if(data.msg=='ok'){
                    this.toastservice.showSuccessToast('注册成功，请进行登录！');

                    this.loginlogin();


                }
                else{
                    this.toastservice.showErrrorToast('注册失败，请重试');
                }
            })
    }


    async loginlogin() {
        const modal = await this.modalController.create(<ModalOptions>{
            component: LoginComponent
        });
        return await modal.present();
    }



}
