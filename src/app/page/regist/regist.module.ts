import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';



import { RegistPageRoutingModule } from './regist-routing.module';

import { RegistPage } from './regist.page';
import {IonicModule} from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistPageRoutingModule

  ],
  declarations: [RegistPage]
})
export class RegistPageModule {}
