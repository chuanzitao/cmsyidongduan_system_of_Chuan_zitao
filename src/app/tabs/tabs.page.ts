import { Component } from '@angular/core';
import {ModalController} from "@ionic/angular";
import {ModalOptions} from '@ionic/core'
import {LoginComponent} from '../components/login/login.component';
import {AuthService} from "../service/auth.service";
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(public modalController: ModalController
      ,private authservice:AuthService
      ,private storage:Storage
 ) {}

    //当软件开始运行时调用presentModal方法，弹出登录窗口
    ngOnInit(){

    }



    ionViewWillEnter(){

        this.storage.get("TOKEN")
            .then((data:any)=>{
                if(data){

                }
                else{
                    this.presentModal();
                }

            })


    }

    //弹出登录窗口
    async presentModal() {
        const modal = await this.modalController.create(<ModalOptions>{
            component: LoginComponent
        });
        return await modal.present();
    }

}
