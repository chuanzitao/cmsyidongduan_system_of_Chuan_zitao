import { Component } from '@angular/core';
import {Carousel} from '../model/Carousel';
import {AuthService} from '../service/auth.service';
import {ConfigService} from '../service/config.service';
import {CarouselService} from '../service/carousel.service';
import {SinglepageService} from '../service/singlepage.service';
import {Singlepage} from '../model/Singlepage';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    cl:Array<Carousel>;
    sl:Array<Singlepage>;
    isregistuser:boolean;
    hosturl:string;
    slideOpts = {
        initialSlide: 1,
        speed: 400
    };

  constructor(private authservice:AuthService
      ,private config:ConfigService
  ,private carouselservice:CarouselService
      ,private singlepageservice:SinglepageService
      ,private router:Router) {
      this.cl=new Array();
      this.sl=new Array();
      this.hosturl=this.config.HOST;
      this.hosturl+="/public/";
      this.isregistuser=false;
  }

    ionViewWillEnter(){
        this.loadCarouselFormServer();
        this.loadSinglepageList();

    }

    loadCarouselFormServer(){
        this.carouselservice.getCarouselList()
            .then((data:any)=>{
                this.cl=new Array();
                this.cl=data;

            })
    }

    loadSinglepageList(){
        this.singlepageservice.getSinglePageList()
            .then((data:any)=>{
                this.sl=new Array();
                this.sl=data;
            })
    }

    //这个用来单个查询
    goTosinglepageDetail(id:string){
        this.router.navigate(['singlepagedetail',{"id":id}])
    }


}