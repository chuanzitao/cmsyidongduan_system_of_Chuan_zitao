import { Component } from '@angular/core';
import {Carousel} from "../model/Carousel";
import {ConfigService} from "../service/config.service";
import {AuthService} from '../service/auth.service';
import {Users} from '../model/Users';
import {ToastService} from '../service/toast.service';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
    user:Users;
    cl:Array<Carousel>;
    isregistuser:boolean;
    hosturl:string;
    slideOpts = {
        initialSlide: 1,
        speed: 400
    };


    constructor(private config:ConfigService
        , private authservice:AuthService
        ,private toastservice:ToastService,
        private sanitizer: DomSanitizer) {
        this.cl=new Array();
        this.hosturl=this.config.HOST;
        this.hosturl+="/public/";
        this.isregistuser=false;
        this.user=new Users();
        this.loadMyUserInfo();
    }


    ionViewWillEnter(){
        this.isRegist();
    }

    photo: SafeResourceUrl;
    async takePicture() {
        const image = await Plugins.Camera.getPhoto({
            quality: 100,
            allowEditing: false,
            resultType: CameraResultType.DataUrl,
            source: CameraSource.Camera
        });

        this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl));
    }


    isRegist(){
        this.authservice.isRegistUser()
            .then((data:any)=>{

                if(data.msg==true){
                    this.isregistuser=true
                }
                else{
                    this.isregistuser=false;
                }


            })


    }



    loadMyUserInfo(){
        this.authservice.getMyUserInfo()
            .then((data:any)=>{
                if(data.msg=='ok'){
                    this.user=new Users();
                    this.user=data.obj;


                }
                else{

                    this.toastservice.showErrrorToast("获取信息异常请重试");
                }


            })

    }


}
