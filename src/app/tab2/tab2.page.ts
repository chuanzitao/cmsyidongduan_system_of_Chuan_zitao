import { Component } from '@angular/core';
import {NewsService} from '../service/news.service';
import {News} from '../model/News';
import {Router} from '@angular/router';
import {MediaService} from '../service/media.service';
import {Media} from '../model/Media';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
    ml:Array<Media>
    newslist:Array<News>
    constructor(private newsservice:NewsService
        ,private router:Router
    ,private mediaservice:MediaService) {
        this.newslist=new Array();
        this.ml=new Array();
    }
//在页面组件没有加载前调用
    ionViewWillEnter(){
        this.loadNewsList();
        this.loadMediaList();
    }

    loadNewsList(){
        this.newsservice.getNewsList()
            .then((data:any)=>{
                this.newslist=new Array();
                this.newslist=data;
            })
    }

    loadMediaList(){
        this.mediaservice.getMediaList("VIDEO")

            .then((data:any)=>{
                this.ml=new Array();
                this.ml=data;

            })
    }

    //这个用来单个查询
    goTonewsDetail(id:string){
        this.router.navigate(['newsdetail',{"id":id}])
    }

    goToNewsDetail(id:string){

        this.router.navigate(['media',{"mediaid":id}])
    }


}