import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'newsdetail',
    loadChildren: () => import('./page/newsdetail/newsdetail.module').then( m => m.NewsdetailPageModule)
  },
  {
    path: 'regist',
    loadChildren: () => import('./page/regist/regist.module').then( m => m.RegistPageModule)
  },
  {
    path: 'p401',
    loadChildren: () => import('./page/p401/p401.module').then( m => m.P401PageModule)
  },
  {
    path: 'p403',
    loadChildren: () => import('./page/p403/p403.module').then( m => m.P403PageModule)
  },
  {
    path: 'singlepagedetail',
    loadChildren: () => import('./page/singlepagedetail/singlepagedetail.module').then( m => m.SinglepagedetailPageModule)
  },
  {
    path: 'media',
    loadChildren: () => import('./page/media/media.module').then( m => m.MediaPageModule)
  },
  {
    path: 'edito',
    loadChildren: () => import('./page/edito/edito.module').then( m => m.EditoPageModule)
  },
  {
    path: 'myfridend',
    loadChildren: () => import('./page/myfridend/myfridend.module').then( m => m.MyfridendPageModule)
  },
  {
    path: 'editfridend',
    loadChildren: () => import('./page/editfridend/editfridend.module').then( m => m.EditfridendPageModule)
  },
  {
    path: 'feed',
    loadChildren: () => import('./page/feed/feed.module').then( m => m.FeedPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./page/about/about.module').then( m => m.AboutPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
