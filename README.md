## 用Java编写的CMS系统移动端简单说明


### 项目简介

- J2EE课程期末项目的移动端
- 用Java编写的简单的CMS（内容管理）系统

### 功能特征

- 移动端具备APP基本特征
- 数据库存储数据，动态管理网站内容
- 具有`网站logo`、`导航区`、`图片轮播区`、`单页区`、`新闻列表区`、`多媒体展示区`、`底部版权说明`等模块。

### 环境依赖

- ionic: 6.6.0
- Node: 12.16.1
- OS: windows x64

### 我的个人技术博客

- [微笑涛声](https://www.cztcms.cn)
- 或者浏览器输入：www.cztcms.cn
